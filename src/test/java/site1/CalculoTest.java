package site1;

import br.com.site1.Calcular;
import junit.framework.TestCase;

public class CalculoTest extends TestCase{
	
	public void testSoma(){
		//passando valores a serem calculados e testados
		float PassaValor1=8;
		float PassaValor2=6;
		float RetornoEsperado=14;
		
		//Executa metodo da classo Calculo e armazena resultado em uma variavel
		float RetornoFeito=Calcular.Soma(PassaValor1,PassaValor2);
		
		//compara o valor retornadoo com o esperado
		assertEquals(RetornoEsperado,RetornoFeito,0);
		
	}

}
