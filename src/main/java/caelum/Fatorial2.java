package caelum;

public class Fatorial2{

	
public static void main (String[] args) {
        long fatorial = 1;
        for (int n = 1; n <= 20; n++) {
            fatorial = fatorial * n;
            System.out.println("fat(" + n + ") = " + fatorial);
        }
    }

}
/* com INT
 fat(1) = 1
fat(2) = 2
fat(3) = 6
fat(4) = 24
fat(5) = 120
fat(6) = 720
fat(7) = 5040
fat(8) = 40320
fat(9) = 362880
fat(10) = 3628800
fat(11) = 39916800
fat(12) = 479001600
fat(13) = 1932053504
fat(14) = 1278945280
fat(15) = 2004310016
fat(16) = 2004189184
fat(17) = -288522240
fat(18) = -898433024
fat(19) = 109641728
fat(20) = -2102132736
 */
/*com LONG
fat(1) = 1
fat(2) = 2
fat(3) = 6
fat(4) = 24
fat(5) = 120
fat(6) = 720
fat(7) = 5040
fat(8) = 40320
fat(9) = 362880
fat(10) = 3628800
fat(11) = 39916800
fat(12) = 479001600
fat(13) = 6227020800
fat(14) = 87178291200
fat(15) = 1307674368000
fat(16) = 20922789888000
fat(17) = 355687428096000
fat(18) = 6402373705728000
fat(19) = 121645100408832000
fat(20) = 2432902008176640000  */