package br.com.site1;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdicionaProdutoServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			 throws ServletException, IOException {
				PrintWriter saida = response.getWriter();
				
				String acao = request.getParameter("acao");
				String nome = request.getParameter("nome");				
				
				if (acao.equals("adicionar")){

					String descricao = request.getParameter("descricao");
					String quantidade_s = request.getParameter("quantidade");
					String valor_s = request.getParameter("valor");
					String caminho_s = request.getParameter("caminho");
					String file_s = request.getParameter("imagem");
					System.out.println(file_s);
					
					File file = new File(caminho_s+"/"+file_s);
					InputStream imagem = new FileInputStream(caminho_s+"/"+file_s);
					
					System.out.println(acao);
					
					//Fazendo comversao para int
					int quantidade = Integer.parseInt(quantidade_s);
					
					//Fazendo comversao para double
					double valor = Double.parseDouble(valor_s);
					
					//Criando produto
					Produto produto = new Produto();
					produto.setNome(nome);
					produto.setDescricao(descricao);
					produto.setQuantidade(quantidade);
					produto.setValor(valor);
					//produto.setImagem(imagem);
					
					Connection con = new ConnectionFactory().getConnetConnection();
					
					String sql = "insert into produto"+"(nome,descricao,quantidade,valor,imagem)"+"values(?,?,?,?,?)";
					try {
						PreparedStatement stmt = con.prepareStatement(sql);
						
						//preenche os valores
						stmt.setString(1, nome);
						stmt.setString(2, descricao);
						stmt.setInt(3, quantidade);
						stmt.setDouble(4, valor);
						stmt.setBinaryStream(5, imagem, (int)file.length());
						
						//executa
						stmt.execute();
						stmt.close();
						
						con.close();
						
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
					//Enviando reposta para o cliente
					saida.println("<html>");
					saida.println("<head>");
					saida.println("<title>Testando o servlet</title>");
					saida.println("</head>");
					saida.println("<body>");
					saida.println("</body>");
					saida.println("<h1>Produto Adicionado:</h1>");
					saida.println("<b>Nome:</b> "+nome+"<br>");
					saida.println("<b>Quantidade:</b> "+ quantidade+"<br>");
					saida.println("<b>Valor: </b> "+valor+"<br>");
					saida.println("<a href='adicionaProduto.html'> VOLTAR PARA TELA DE ADICIONAR</a><br>");
					saida.println("<a href='servlet'> VOLTAR PARA A TELA INCIAL</a>");
					saida.println("</html>");
		}	else{
		
			Connection con = new ConnectionFactory().getConnetConnection();
			String sql = "delete from produto where nome=?";
			
			try {
				PreparedStatement stmt = con.prepareStatement(sql);
				
				stmt.setString(1, nome);
				
				//executa
				stmt.execute();
				stmt.close();
					
				con.close();

				saida.println("<html>");
				saida.println("<META http-equiv='refresh' content='0;URL=servlet'>"); 
				saida.println("</html>");
				
			} catch (SQLException e) {
				e.printStackTrace();
			} 
					
			
		}
			
		

				}
				}
