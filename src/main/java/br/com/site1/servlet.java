package br.com.site1;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;


public class servlet extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			 throws ServletException, IOException {
				PrintWriter saida = response.getWriter();
			
				saida.println("<html>");
				saida.println("<head>");
				saida.println("<title>Testando o servlet</title>");
				saida.println("</head>");
				saida.println("<body>");
				saida.println("</body>");
				saida.println("<h1>Lista de Produtos</h1>");
				saida.println("<table border=10 >");
				saida.println("<tr ><td width=150 ><b>Nome</b></td><td width=300 ><b>Descricao</b></td><td><b>Quantidade</b></td><td><b>Valor (R$)</b></td><td width=300 ><b>Imagem</b></td></td><td><b>:-)</b></td>");

				//pega a conexao e o statement
				Connection con = new ConnectionFactory().getConnetConnection();
				try {
					PreparedStatement stmt = con.prepareStatement("select * from produto");
					
					//executa um select
					ResultSet rs = stmt.executeQuery();
					
					//itera no REsultSet
					while (rs.next()){
						String nome = rs.getString("nome");
						String descricao = rs.getString("descricao");
						int quantidade = rs.getInt("quantidade");
						double valor = rs.getDouble("valor");
						byte[] imagem = rs.getBytes("imagem");
						
						saida.println("<tr>");
						saida.println("<td>"+nome + "</td>");
						saida.println("<td>"+descricao+"</td>");
						saida.println("<td>"+quantidade+"</td>");
						saida.println("<td>"+valor+"</td>");
						
						if (imagem != null){
		

							try{
//								FileOutputStream fos = new FileOutputStream("/projetos/jboss/jboss-as-7.1.1.Final/standalone/deployments/site1.war/images/"+nome+".png");
								FileOutputStream fos = new FileOutputStream("C:/Program Files (x86)/jboss-as-7.1.1.Final/standalone/deployments/site1.war/images/"+nome+".png");
								fos.write(imagem);
								FileDescriptor fd = fos.getFD();
								fos.flush();
								fd.sync();
								fos.close();


							}catch(Exception e){
								String erro = e.toString();
								System.out.println(nome + erro);
							}
							
							saida.println("<td><img src="+"'http://localhost:8181/site1/images/"+nome+".png'"+"></td>");
							
 
							
						} else {
							saida.println("<td>-</td>");
						}

						saida.println("<td><form action='adicionaProduto' method='get'><br><input type='hidden' value='excluir' name='acao'/><input type='hidden' value="+nome+" name='nome'/> <br/> <br/><input type='submit' value='Excluir'></form></td>");
						saida.println("</tr>");
					}
					
					
					rs.close();
					stmt.close();
					con.close();
					
				} catch (SQLException e) {
					//e.printStackTrace();
					Logger.getLogger(e.toString());
				}
				saida.println("</td>");

				saida.println("</table>");
				saida.println("<a href='adicionaProduto.html'> VOLTAR PARA TELA DE ADICIONAR</a><br>");
				saida.println("<a href='index.html'> VOLTAR PARA A TELA INCIAL</a>");
				saida.println("</html>");
				

				
			}

}
